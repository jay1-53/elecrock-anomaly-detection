# elecrock-anomaly-detection
# —
## Pre-requisites and environment
The `network-anomaly.py` assumes that the hardware has **numpy** and **psutil** installed in the machine. Furthermore, these pre-requisites will only be attained if there's a python installed in the hardware.

To install numpy:
> pip install numpy

To install psutil
> pip install psutil

## The network anomaly
In every 60 seconds, the program will automatically store the information gathered under the hardware activities. The following will be gathered on the user's computer:
### CPU Usage
By using the `psutil.cpu_percent()`, the software can gather the CPU activity whilst the program is running.

### Storage Usage
Storage usage will be monitored constantly along with its total, used, and free space. This was done by using `psutil.disk_usage('/')`

### Network Usage
The network bandwidth activity which are sending and receiving is being monitored whilst being deducted to its previous value to precisely gather the amount of bandwidth usage in measure of *bytes*.

## Executing the program
For the meantime, simply running `network-anomaly.py` on an IDE or any compiler would yield results, and the cached informations will be sent to `logs/{process-type}_{datetime}`, the same directory as where the executable program is.
