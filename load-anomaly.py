# load numpy array from npz file
from numpy import load
from matplotlib import pyplot as plt
# load dict of arrays
dict_data = load('logs/sent_usage-00-09-13-965781.npz')
# extract the first array
sent = dict_data['sent']
timestamp = dict_data['time']

## for debugging
# for i in range(len(timestamp)):
#     print (i, end = " ")
#     print (timestamp[i])

plt.plot(timestamp, sent)